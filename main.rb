require 'uri'
require 'net/http'
require 'json'

require 'slack/incoming/webhooks'
require 'dotenv'

Dotenv.load
slack_url = ENV['SLACK_URL']

json_data = open('target.json') do |io|
  # JSON.load(io, symbolize_names: true)
  JSON.load(io)
end

json_data.each do |target|

  http_status = Net::HTTP.get_response(URI.parse(target['url'])).code
  
  exit if http_status == target['ok_status']
  
  target['attachments'][0]['tile_link'] = target['url']
  target['attachments'][0]['text'] = "HTTPS Status: #{http_status}"

  slack = Slack::Incoming::Webhooks.new( 
    slack_url,
    username: target['username'],
    attachments: target['attachments']
  )

  slack.post(target['post_message'])
end
