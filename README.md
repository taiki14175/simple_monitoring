# Simple Monitoring

## Usage

Change '.env' to incoming-webhook URL.
```
SLACK_URL=https://hooks.slack.com/services/xxx/xxx/xxx
```

Change notification for your environment.
```
[
  {
    "ok_status": "200",
    "url": "http://xxx.xxx.xxx.xxx",
    "username": "from Monitoring Server",
    "post_message": "Server is down!",
    "attachments": [{
      "color": "danger",
      "pretext": "Check and fix server status.",
      "title": "Apache" 
    }]
  }
]
```

Install Gem files.
```
bundle install
```

Send slack notification.
```
ruby main.rb
```

## Cron Setting

See 'crontab-sample' and 'cron'.

Be careful ruby path.
